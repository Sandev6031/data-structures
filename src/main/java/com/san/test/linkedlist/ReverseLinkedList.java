package com.san.test.linkedlist;

public class ReverseLinkedList {

	
	public static Node reverse(Node linkedList) {
		if(linkedList == null)
			return null;
		reverse(linkedList.right);
		Node temp = linkedList.right;
		temp.right = linkedList;
		return temp;
	}

	public static void main(String[] args) {
	    Node linkedList = new Node(0);
    	Node temp =linkedList;
	    for(int a=1;a<10;a++) {
	    	temp.right = new Node(a);
	    	temp= temp.right;
	    }

		System.out.println(linkedList);
		System.out.println(reverse(linkedList));
	}

}

