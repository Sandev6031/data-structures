package com.san.test.linkedlist;

public class Node {
	int data;
	Node right;

	// Function to create a new binary tree node having a given key
	public Node(int key) {
		data = key;
		right = null;
	}

	@Override
	public String toString() {
		return  data + "," + right;
	}
	

}
