package com.san.test.binary;

public class findLargestNode {

	// Recursive function to perform inorder traversal on the tree
	public static int findMax(Node root) {
		// return if the current node is empty
		if (root == null) {
			return 0;
		}

		int largeLeft = findMax(root.left);
		int largeRight = findMax(root.right);
		if( root.data > largeRight && root.data > largeLeft) {
			return root.data;
		} else if (largeLeft > largeRight) {
			return largeLeft;
		} else {
			return largeRight;
		}
	}

	public static void main(String[] args) {
	      /* Construct the following tree
		        1
		      /   \
		     /     \
		    2       3
		   /      /   \
		  /      /     \
		 4      5       6
		       / \
		      /   \
		     7     8
	     */
		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.left.left = new Node(4);
		root.right.left = new Node(5);
		root.right.right = new Node(6);
		root.right.left.left = new Node(7);
		root.right.left.right = new Node(8);

		System.out.print(findMax(root));
	}

}

